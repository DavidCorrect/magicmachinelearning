import rnn
import json
import codecs
import numpy as np

class BiRNN:

  #********************************************************************
  # Function:     __init__
  #
  # Description:  Intializes class. Can either use weight dictionary
  #               or randomly initializes on defined size
  #
  # Parameters:   pf      - dictoinary of weights for forward model
  #               pb      - dictoinary of weights for backward model
  #               x_dim   - input dimension
  #               y_dim   - output dimension
  #               h_dim   - hidden layer dimension
  #********************************************************************
  def __init__(self, pf=None, pb=None, x_dim=None, y_dim=None, h_dim=None):
    if(pf and pb):
      self.forward = rnn.RNN(pf)
      self.backward = rnn.RNN(pb)
    elif x_dim:
      self.forward = rnn.RNN(x_dim=x_dim, y_dim=y_dim, h_dim=h_dim)
      self.backward = rnn.RNN(x_dim=x_dim, y_dim=y_dim, h_dim=h_dim)
  
  #********************************************************************
  # Function:     Prediction
  #
  # Description:  makes prediciton given input sequence
  #
  # Parameters:   x - input sequence
  #
  # Returned:     sequnece of predictions
  #********************************************************************
  def prediction(self, x):
    af_prev = np.zeros(self.forward.h_dim)
    ab_prev = np.zeros(self.backward.h_dim)
    yf = []
    yb = []
    y = []
    a_avg = []

    for xt in x:
      af_prev, y_pred = self.forward.runCell(xt, af_prev)
      yf.append(y_pred)
    
    for xt in np.flip(x):
      ab_prev, y_pred = self.backward.runCell(xt, ab_prev)
      yb.append(y_pred)

    for i in range(len(self.forward.a)):
      a_avg.append((self.forward.a[i] + self.backward.a[i]) / 2)  

    for a_curr in a_avg:
      y.append(rnn.relu(np.dot(self.forward.Wya, a_curr) + self.forward.by))
    
    return y
  
  #********************************************************************
  # Function:     saveModel
  #
  # Description:  saves model to json file
  #
  # Parameters:   file- file path
  #
  # Returned:     none
  #********************************************************************
  def saveModel(self, file):
    predfDict = {
      'Wax': self.forward.Wax.tolist(),
      'Waa': self.forward.Waa.tolist(),
      'Wya': self.forward.Wya.tolist(),
      'ba': self.forward.ba,
      'by': self.forward.by
    }

    predbDict = {
      'Wax': self.backward.Wax.tolist(),
      'Waa': self.backward.Waa.tolist(),
      'Wya': self.backward.Wya.tolist(),
      'ba': self.backward.ba,
      'by': self.backward.by
    }

    predDict = {
      'forward': predfDict,
      'backward': predbDict
    }

    with open(file, 'w') as outfile:
      json.dump(predDict, outfile, indent=2)
  
  #********************************************************************
  # Function:     openModel
  #
  # Description:  opens model from saved file and overwrites current
  #
  # Parameters:   file - file path
  #
  # Returned:     none
  #********************************************************************
  def openModel(self, file):
    obj_text = codecs.open(file, 'r', encoding='utf-8').read()
    json_data = json.loads(obj_text)

    self.forward.Waa = np.array(json_data['forward']['Waa'])
    self.forward.Wax = np.array(json_data['forward']['Wax'])
    self.forward.Wya = np.array(json_data['forward']['Wya'])
    self.forward.ba = json_data['forward']['ba']
    self.forward.by = json_data['forward']['by']

    self.backward.Waa = np.array(json_data['backward']['Waa'])
    self.backward.Wax = np.array(json_data['backward']['Wax'])
    self.backward.Wya = np.array(json_data['backward']['Wya'])
    self.backward.ba = json_data['backward']['ba']
    self.backward.by = json_data['backward']['by']

  #********************************************************************
  # Function:     falttenWm
  #
  # Description:  Flattens weight matrices into 1D vecotr
  #
  # Parameters:   none
  #
  # Returned:     1D vector rep of WMs
  #********************************************************************
  def flattenWm(self):
    flat = np.array([])

    flat = np.append(flat, self.forward.Wax.flatten())
    flat = np.append(flat, self.forward.Waa.flatten())
    flat = np.append(flat, self.forward.Wya.flatten())
    flat = np.append(flat, self.forward.ba)
    flat = np.append(flat, self.forward.by)

    flat = np.append(flat, self.backward.Wax.flatten())
    flat = np.append(flat, self.backward.Waa.flatten())
    flat = np.append(flat, self.backward.Wya.flatten())
    flat = np.append(flat, self.backward.ba)
    flat = np.append(flat, self.backward.by)

    return flat

  #********************************************************************
  # Function:     unFlattenWm
  #
  # Description:  Redoes WMs given a flattened 1D
  #
  # Parameters:   vec - 1d vector to apply to wm
  #
  # Returned:     none
  #********************************************************************
  def unFlattenWm(self, vec):
    
    split = np.split(vec, 2)

    self.forward.by = split[0][-1]
    self.forward.ba = split[0][-2]

    np.delete(split[0], -1)
    np.delete(split[0], -1)

    for i in range(len(self.forward.Wax)):
      self.forward.Wax[i] = split[0][:len(self.forward.Wax[i])]
    
    for i in range(len(self.forward.Wax)):
      np.delete(split[0], np.s_[:len(self.forward.Wax[i])])
    
    for i in range(len(self.forward.Waa)):
      self.forward.Wax[i] = split[0][:len(self.forward.Wax[i])]
    
    for i in range(len(self.forward.Waa)):
      np.delete(split[0], np.s_[:len(self.forward.Wax[i])])

    for i in range(len(self.forward.Wya)):
      self.forward.Wax[i] = split[0][:len(self.forward.Wax[i])]
    
    for i in range(len(self.forward.Wya)):
      np.delete(split[0], np.s_[:len(self.forward.Wax[i])])

  
    self.backward.by = split[1][-1]
    self.backward.ba = split[1][-2]

    np.delete(split[1], -1)
    np.delete(split[1], -1)

    for i in range(len(self.backward.Wax)):
      self.backward.Wax[i] = split[1][:len(self.backward.Wax[i])]
    
    for i in range(len(self.backward.Wax)):
      np.delete(split[1], np.s_[:len(self.backward.Wax[i])])
    
    for i in range(len(self.backward.Waa)):
      self.backward.Wax[i] = split[1][:len(self.backward.Wax[i])]
    
    for i in range(len(self.backward.Waa)):
      np.delete(split[1], np.s_[:len(self.backward.Wax[i])])

    for i in range(len(self.backward.Wya)):
      self.backward.Wax[i] = split[1][:len(self.backward.Wax[i])]
    
    for i in range(len(self.forward.Wya)):
      np.delete(split[1], np.s_[:len(self.backward.Wax[i])])