import json
import codecs

def fixIndent(filePath):
  obj_text = codecs.open(filePath, 'r', encoding='utf-8').read()
  json_data = json.loads(obj_text)

  with open(filePath, 'w') as outfile:
      json.dump(json_data, outfile, indent=2)