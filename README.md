# The Magic of Machine Learning

## Overview 
The Magic of Machine Learning is a Python project designed to train models to decide attackers in the TCG Magic: The Gathering.

### Python modules
All neccesary modules can be install via the packInstall.py file. If you wish to install them manually they are listed below:

- numpy
- scipy

### XMage
The project is based on output given by the open source Project XMage. In our instance, we forked XMage and add JSON output of the boardstate that this project reads from.

### JSON Paths
The project uses hard coded paths to the boardstate JSON and the output of training models
