import os
import sys
import genticAlg as ga
import numpy as np
import birnn


x_dim = 10
y_dim = 1
h_dim = 5

numGens = int(sys.argv[1])
numChildren = int(sys.argv[2])

# Change for your machine
prePath = 'C:/Users/dwrig/Desktop/fitnessPre.json'
postPath = 'C:/Users/dwrig/Desktop/fitnessPost.json'

def runGame(childNum, genNum):
  cmdString = 'cmd /c "mvn -Dtest=TestPlayRandomGame#playGames test"'
  os.chdir('C:/Projects/magicmachinelearningxmage/Mage.Tests')
  os.system(cmdString)
  open("C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/curr_child.txt").write(str(childNum) + ' ' + str(genNum))


fitness = []

for x in range (numGens):
  for y in range (numChildren):
    ga.initJson(prePath)
    ga.initJson(postPath)

    runGame(y, x)

    fitness.append(ga.getMatchFittness(prePath, postPath))
    ga.saveFitness('C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_fitness/gen' + str(x) + 'ind' + str(y) + '.json', ga.getMatchFittness(prePath, postPath))
  
  fitness = np.array(fitness)

  # Indicies of the top 2
  ind = np.argpartition(fitness, -2)[-2:]

  par1 = birnn.BiRNN(x_dim=x_dim, y_dim=y_dim, h_dim=h_dim)
  par2 = birnn.BiRNN(x_dim=x_dim, y_dim=y_dim, h_dim=h_dim)
  par1.openModel('C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/gen' + str(x) + '/ind' + str(ind[0]) + '.json')
  par1.openModel('C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/gen' + str(x) + '/ind' + str(ind[1]) + '.json')

  # Create Next Generation
  ga.nextGen(par1.flattenWm(), par2.flattenWm())

  

#print after action data here
