import numpy as np
import json
import magic
import fixJson
import pandas as pd

pop_size = 10
gen_size = 100

def getdata(path):
  data = magic.getJson(path)

  return data['fitness']

def remapData():
  fitness = np.array([])

  for i in range(gen_size):
    gen = np.array([])

    for j in range(pop_size):
      gen = np.append(gen, getdata('C:/Users/dwrig/Desktop/model_fitness/gen' + str(i) + 'ind' + str(j) + '.json'))
    
    fitness = np.append(fitness, gen)

  fitness = np.reshape(fitness, (gen_size, pop_size))

  avg = []
  argmax = []

  for x in fitness:
    avg.append(np.average(x))
    argmax.append(x[int(np.argmax(x))])
  
  return avg, argmax
    
def dumpToJson(avg, argmax):
  data = {}
  data['avg'] = avg
  data['argmax'] = argmax

  with open('./fitAnalysis.json', 'w') as outfile:
      json.dump(data, outfile)

  fixJson.fixIndent('./fitAnalysis.json')


def dumpToCSV(path):
  avg, argmax = remapData()

  df = pd.DataFrame({ 'avg' : avg,
                      'argmax:': argmax })
  
  df.to_csv(path, index=False)


dumpToCSV('./fitData.csv')

