import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

import numpy as np
import matplotlib as plot
import pandas as pd
import json


class TensorModel:
    #--------------------------------------------------------------------------
    # Function:     __init__
    #
    # Description:  Initializes model object
    #
    # Arguments:    inputSize   - Input layer size
    #               path        - file path to saved model
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def __init__(self, inputSize=None, path=None):
        if (path != None):
            self.__model = keras.models.load_model(path)
        else:
            self.__model = keras.Sequential([
                keras.layers.Flatten(input_shape=(inputSize,)),
                keras.layers.Dense(16, activation=tf.nn.relu),
                keras.layers.Dense(16, activation=tf.nn.relu),
                keras.layers.Dense(1, activation=tf.nn.sigmoid),
            ])

            self.__compileModel()

    #--------------------------------------------------------------------------
    # Function:     compileModel
    #
    # Description:  Compiles model optimizer, loss function, and metrics
    #               
    #               Optmizer        - How model is updated
    #               Loss function   - measures model accuracy
    #               Metrics         - monitors training/testing steps
    #               
    # Arguments:    none
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def __compileModel(self):
        self.__model.compile(
            optimizer='adam',
            loss='binary_crossentropy',
            metrics=['accuracy']
        )
    
    #--------------------------------------------------------------------------
    # Function:     fitModel
    #
    # Description:  trains model by iterating over given epochs
    #
    # Arguments:    csv     - csv file path
    #               iter    - how many iteration to run training
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def fitModel(self, csv, iter):
        df = pd.read_csv(csv)
        
        # Target is binary classification of yes or no choice
        target = df.pop('target')

        dataset = tf.data.Dataset.from_tensor_slices((df.values, target.values))
        train_data = dataset.shuffle(len(df)).batch(1)

        self.__model.fit(train_data, epochs=iter)

    #--------------------------------------------------------------------------
    # Function:     saveModel
    #
    # Description:  saves model using the given path. Use either the folder to
    #               to create or use the .h5 extension for a single file
    #
    # Arguments:    path - path to file or folder to save
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def saveModel(self, path):
        self.__model.save(path)

    #--------------------------------------------------------------------------
    # Function:     getAction
    #
    # Description:  gets actions to take from the model. Return list of binary
    #               choices. Model prediction returns probability. Choice
    #               will be yes if over certain threshhold
    #
    # Arguments:    data - numpy array that fits the input layer of the model
    #
    # Returned:     list of binary choices. 0 = no, 1 = yes
    #--------------------------------------------------------------------------
    def getAction(self, data):
        if self.__model.predict(np.array([data,])) > 0.9:
            return 1
        else:
            return 0

    #--------------------------------------------------------------------------
    # Function:     getJson
    #
    # Description:  Retreives json data for input layer
    #
    # Arguments:    filePath    - path to json file
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def getJSON(self, filePath):
        with open(filePath, "r") as file:
            return json.load(file)



class AttackModel(TensorModel):
    #--------------------------------------------------------------------------
    # Function:     __init__
    #
    # Description:  Initializes model object
    #
    # Arguments:    inputSize   - Input layer size
    #               path        - file path to saved model
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def __init__(self, inputSize=None, path=None):
        super().__init__(inputSize=inputSize, path=path)

    #--------------------------------------------------------------------------
    # Function:     getInput
    #
    # Description:  gets attackers json and return list of input layer arrays
    #
    # Arguments:    none
    #
    # Returned:     list of numpy arrays to fit input layer of attack model
    #--------------------------------------------------------------------------
    def getInput(self):
        data = self.getJSON("test.json")
        power = []
        tough = []

        attInput = []

        for deff in data["oppDefenders"]:
            power.append(int(deff["power"]))
            tough.append(int(deff["toughness"]))

        power = np.array(power)
        tough = np.array(tough)

        for cre in data["myAttackers"]:
            attInput.append(
                np.array([
                    int(data["myLife"]), int(data["oppLife"]), 
                    int(cre["power"]), int(cre["toughness"]),
                    len(data["oppDefenders"]),
                    np.argmax(power), np.argmin(power), np.mean(power),
                    np.argmax(tough), np.argmin(tough), np.mean(tough)
                ])
            )
        
        return attInput



class BidirectionalRNN:
    #--------------------------------------------------------------------------
    # Function:     __init__
    #
    # Description:  Initializes model object
    #
    # Arguments:    inputSize   - Input layer size
    #               path        - file path to saved model
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def __init__(self, path=None):
        model = keras.Sequential()

        model.add
        (
            layers.Bidirectional(layers.LSTM())
        )

    #--------------------------------------------------------------------------
    # Function:     getInput
    #
    # Description:  gets blockers json and return list of input layer arrays
    #
    # Arguments:    none
    #
    # Returned:     list of numpy arrays to fit input layer of attack model
    #--------------------------------------------------------------------------
    def getInput(self):
        return 1



class CardModel(TensorModel):
    #--------------------------------------------------------------------------
    # Function:     __init__
    #
    # Description:  Initializes model object
    #
    # Arguments:    inputSize   - Input layer size
    #               path        - file path to saved model
    #
    # Returned:     none
    #--------------------------------------------------------------------------
    def __init__(self, inputSize=None, path=None):
        super().__init__(inputSize=inputSize, path=path)

    #--------------------------------------------------------------------------
    # Function:     getInput
    #
    # Description:  gets card json and return list of input layer arrays
    #
    # Arguments:    none
    #
    # Returned:     list of numpy arrays to fit input layer of attack model
    #--------------------------------------------------------------------------
    def getInput(self):
        return 1 