import numpy as np
import tensorflow
from tensorflow import nn
import random


class WeightMatrix:
  def __init__(self, layerSize):
    self.weights = np.array([])

    for _ in range(layerSize):
      np.append(self.weights, [random.randint(1, 1000) / 10])


class Node:
  def __init__(self, layerSize):
    self.weightMatrix = WeightMatrix(layerSize)

    self.value = 0

  def getNextTensor(self):
    return self.weightMatrix.weights * self.value


class Layer:
  def __init__(self, size):
    self.nodes = np.array([])

    for _ in range(size):
      np.append(self.nodes, [Node(size)])


class Cell:
  def __init__(self, layers):
    self.layers = np.array([])
    
    for _ in range(layers):
      np.append(self.layers, [Layer(10)])

  def getX_t(self):
    # run through each layer to get next tensor
    # return total linear algerbra-ed value
    return 0
  
  def getH_t(self):
    # multiple h by weight matrix
    # add to getX_t
    # run through tanh and return
    return 0

class Network:
  def __init__(self, x, y, h):
    # U is the weight matrix for weights between input and hidden layers
    # V is the weight matrix for weights between hidden and output layers
    # W is the weight matrix for shared weights in the RNN layer (hidden layer)

    self.U = np.random.uniform(0, 1, (h, x))
    self.W = np.random.uniform(0, 1, (h, h))
    self.V = np.random.uniform(0, 1, (y, h))

    self.cell = Cell(2)

  def getPrediction(self):
    # multiple weight matrix by h
    # return
    return 0