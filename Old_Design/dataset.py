import numpy as np
import pandas as pd
import random


def randStats():
    stats = np.array([random.randint(1, 5), random.randint(1, 5), random.randint(1, 5)])
    stats = np.sort(stats)
    
    return stats


n = 5
table = []

for i in range(n - 1):
    powStats = randStats()
    toughStats = randStats()

    # rand num creatures
    # if 0 then not this
    
    row = np.array([
        random.randint(1, 20), random.randint(1, 20),   # myLife / oppLife
        random.randint(0, 5), random.randint(1, 5),     # my Pow/Tough
        powStats[2], powStats[0], powStats[1],          # max/min/avg pow
        toughStats[2], toughStats[0], toughStats[1]     # max/min/avg tough
    ])

    table.append(row)

table = np.array(table)

df = pd.DataFrame(table, columns=['myLife', 'oppLife', 'myPow', 'myTough', 'maxP', 'minP', 'avgP', 'maxT', 'minT', 'avgT'])

df.to_csv('attTrain.csv', sep=',')

print(df)