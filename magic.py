import json
import codecs
import numpy as np

#********************************************************************
# Function:     getJson
#
# Description:  Retrieves json file
#
# Parameters:   filePath - path to file
#
# Returned:     json object
#********************************************************************
def getJson(filePath):
  obj_text = codecs.open(filePath, 'r', encoding='utf-8').read()
  json_data = json.loads(obj_text)

  return json_data

#********************************************************************
# Function:     getAttackTensor
#
# Description:  Converts XMage json to input tensor for attack model
#
# Parameters:   filePath - path to input file
#
# Returned:     input tensor for attack model
#********************************************************************
def getAttackTensor(filePath):
  data = getJson(filePath)
  
  power = []
  tough = []

  for deff in data['oppDefenders']:
    power.append(int(deff['power']))
    tough.append(int(deff['toughness']))

  if len(power) != 0:
    power = np.array(power)
    tough = np.array(tough)
  else:
    power = [0]
    tough = [0]

  sequence = []

  for cre in data['aiAttackers']:
    sequence.append(
      np.array([
        data['aiLife'],
        data['oppLife'],
        np.argmax(power),
        np.argmax(tough),
        np.argmin(power),
        np.argmin(tough),
        np.average(power),
        np.average(tough),
        int(cre['power']),
        int(cre['toughness'])
      ])
    )

  return np.array(sequence)


#********************************************************************
# Function:     getBlockTensor
#
# Description:  Converts XMage json to input tensor for block model
#
# Parameters:   filePath - path to input file
#
# Returned:     input tensor for block model, 
#               sequence of attackers,
#               sequence of blockers
#********************************************************************
def getBlockTensor(filePath):
  data = getJson(filePath)

  bPower = []
  bTough = []
  block = []

  for cre in data['aiBlockers']:
    bPower.append(cre['power'])
    bTough.append(cre['toughness'])
    block.append([cre['power'], cre['toughness']])
  
  if len(bPower) != 0:
    bPower = np.array(bPower)
    bTough = np.array(bTough)
  else:
    bPower = [0]
    bTough = [0]

  att = []

  for cre in data['oppAttackers']:
    att.append([cre['power'], cre['toughness']])

  sequence = []
  
  for cre in data['oppAttackers']:
    sequence.append(
      np.array([
        data['aiLife'],
        data['oppLife'],
        np.argmax(bPower),
        np.argmax(bTough),
        np.argmin(bPower),
        np.argmin(bTough),
        np.average(bPower),
        np.average(bTough),
        int(cre['power']),
        int(cre['toughness'])
      ])
    )

  return sequence, att, block

#********************************************************************
# Function:     get2BlockTensor
#
# Description:  Consturcts tensor for second block model
#
# Parameters:   modelY    - output from 1st model
#               blockers  - sequence of blockers
#
# Returned:     input sequence for second blocking model
#********************************************************************
def get2BlockTensor(y, att, blockers):
  sequence = []

  for i in range(len(att)):
    if (y[i][0]):
      for j in range(len(blockers)):
        sequence.append(np.array([att[i][0], att[i][1], blockers[j][0], blockers[j][1]]))
  
  return sequence