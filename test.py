import rnn
import birnn
import fixJson
import magic
import scipy as sp
import numpy as np


zeros = []
nonzeros = []

def runModelTest():
  model = rnn.RNN(x_dim=10, y_dim=1, h_dim=5)

  x = magic.getAttackTensor('C:/Users/dwrig/Desktop/attackers.json')

  pred = model.prediction(x)

  for y in pred:
    if y == 0:
      zeros.append(y)
    else:
      nonzeros.append(y)
  
  #model.saveModel('C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/att_model.json')

'''
for _ in range(10):
  runModelTest()

  if len(nonzeros) > 1:
    break

print('Zeros: ' + str(len(zeros)))
print('Nonzeros: ' + str(len(nonzeros)))
'''

print('Model Test')
print('----------')

model = rnn.RNN(x_dim=10, y_dim=1, h_dim=5)
model2 = rnn.RNN(x_dim=10, y_dim=1, h_dim=5)

model.openModel('./model_json/att_model.json')

model2.unFlattenWm(model.flattenWm())

model.saveModel('./model_json/test_att_model.json')
