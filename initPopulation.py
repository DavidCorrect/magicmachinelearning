import birnn
import os

pop_size = 10

for i in range(pop_size):
  model = birnn.BiRNN(x_dim=10, y_dim=1, h_dim=5)

  if not os.path.exists('./model_json/gen0'):
    os.makedirs('./model_json/gen0')

  model.saveModel("./model_json/gen0/ind" + str(i) + ".json")