import numpy as np
import math
import scipy.special as sp
import json
import codecs
import random
import fixJson

# https://medium.com/x8-the-ai-community/building-a-recurrent-neural-network-from-scratch-f7e94251cc80

#********************************************************************
# Function:     relu
#
# Description:  relu activation functions
#
# Parameters:   X - vector
#
# Returned:     0 or X
#********************************************************************
def relu(X):
  return np.maximum(0,X)



class RNN:
  #********************************************************************
  # Function:     __init__
  #
  # Description:  Intializes class. Can either use weight dictionary
  #               or randomly initializes on defined size
  #
  # Parameters:   paramters - dictoinary of weights for model
  #               x_dim     - input dimension
  #               y_dim     - output dimension
  #               h_dim     - hidden layer dimension
  #********************************************************************
  def __init__(self, parameters=None, x_dim=None, y_dim=None, h_dim=None):
    if parameters:
      self.Wax = np.array(parameters['Wax'])
      self.Waa = np.array(parameters['Waa'])
      self.Wya = np.array(parameters['Wya'])
      self.ba = np.array(parameters['ba'])
      self.by = np.array(parameters['by'])
    elif x_dim:
      # Randomly initilizes weights based on dimension sizes: (-1, 1)
      self.Wax = np.random.rand(h_dim, x_dim) * [-1,1][random.randrange(2)]
      self.Waa = np.random.rand(h_dim, h_dim) * [-1,1][random.randrange(2)]
      self.Wya = np.random.rand(y_dim, h_dim) * [-1,1][random.randrange(2)]
      self.ba = np.random.rand() * [-1,1][random.randrange(2)]
      self.by = np.random.rand() * [-1,1][random.randrange(2)]
      self.h_dim = h_dim
    
    self.a = []

  #********************************************************************
  # Function:     runCell
  #
  # Description:  1 iteration or rnn Cell
  #
  # Parameters:   xt      - input at time t
  #               a_prev  - previous hidden state
  #
  # Returned:     next hidden state, prediction at time t
  #********************************************************************
  def runCell(self, xt, a_prev):
    a_next = np.tanh(np.dot(self.Wax, xt) + np.dot(self.Waa, a_prev) + self.ba)

    self.a.append(a_next)

    yt_pred = relu(np.dot(self.Wya, a_next) + self.by)

    # cache = (a_next, a_prev, xt, parameters)

    return a_next, yt_pred

  #********************************************************************
  # Function:     prediction
  #
  # Description:  runs cell for entire input sequence
  #
  # Parameters:   x - input sequence
  #
  # Returned:     prediction sequence
  #********************************************************************
  def prediction(self, x):
    a_prev = np.zeros(self.h_dim)
    y = []

    for xt in x:
      a_prev, y_pred = self.runCell(xt, a_prev)
      y.append(y_pred)
    
    return y

  #********************************************************************
  # Function:     falttenWm
  #
  # Description:  Flattens weight matrices into 1D vecotr
  #
  # Parameters:   none
  #
  # Returned:     1D vector rep of WMs
  #********************************************************************
  def flattenWm(self):
    flat = np.array([])

    flat = np.append(flat, self.Wax.flatten())
    flat = np.append(flat, self.Waa.flatten())
    flat = np.append(flat, self.Wya.flatten())
    flat = np.append(flat, self.ba)
    flat = np.append(flat, self.by)

    return flat

  #********************************************************************
  # Function:     unFlattenWm
  #
  # Description:  Redoes WMs given a flattened 1D
  #
  # Parameters:   vec - 1d vector to apply to wm
  #
  # Returned:     none
  #********************************************************************
  def unFlattenWm(self, vec):
    self.by = vec[-1]
    self.ba = vec[-2]

    np.delete(vec, -1)
    np.delete(vec, -1)

    for i in range(len(self.Wax)):
      self.Wax[i] = vec[:len(self.Wax[i])]
    
    for i in range(len(self.Wax)):
      np.delete(vec, np.s_[:len(self.Wax[i])])
    
    for i in range(len(self.Waa)):
      self.Wax[i] = vec[:len(self.Wax[i])]
    
    for i in range(len(self.Waa)):
      np.delete(vec, np.s_[:len(self.Wax[i])])

    for i in range(len(self.Wya)):
      self.Wax[i] = vec[:len(self.Wax[i])]
    
    for i in range(len(self.Wya)):
      np.delete(vec, np.s_[:len(self.Wax[i])])


  #********************************************************************
  # Function:     saveModel
  #
  # Description:  saves model to json file
  #
  # Parameters:   file- file path
  #
  # Returned:     none
  #********************************************************************
  def saveModel(self, file):
    predDict = {
      'Wax': self.Wax.tolist(),
      'Waa': self.Waa.tolist(),
      'Wya': self.Wya.tolist(),
      'ba': self.ba,
      'by': self.by
    }

    with open(file, 'w') as outfile:
      json.dump(predDict, outfile, indent=2)
  
  #********************************************************************
  # Function:     openModel
  #
  # Description:  opens model from saved file and overwrites current
  #
  # Parameters:   file - file path
  #
  # Returned:     none
  #********************************************************************
  def openModel(self, file):
    obj_text = codecs.open(file, 'r', encoding='utf-8').read()
    json_data = json.loads(obj_text)
    
    self.Waa = np.array(json_data['Waa'])
    self.Wax = np.array(json_data['Wax'])
    self.Wya = np.array(json_data['Wya'])
    self.ba = json_data['ba']
    self.by = json_data['by']