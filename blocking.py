import birnn
import magic
import scipy as sp


model_1 = birnn.BiRNN(x_dim=10, y_dim=1, h_dim=5)
model_2 = birnn.BiRNN(x_dim=4, y_dim=1, h_dim=5)

model_1.openModel("C:/Users/dwrig/Desktop/Code/magicmachinelearning/model_json/block_model_1.json") #C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/block_model_1.json
model_2.openModel("C:/Users/dwrig/Desktop/Code/magicmachinelearning/model_json/block_model_2.json")  #C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/block_model_2.json

# Path will need update per machine
x, att, block = magic.getBlockTensor('C:/Users/dwrig/Desktop/blocking.json')

guess = model_1.prediction(x)

pred = model_2.prediction(magic.get2BlockTensor(sp.special.softmax(guess), att, block))


for seq in pred:
  print(seq)