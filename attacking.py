import rnn
import birnn
import magic
import codecs


model = birnn.BiRNN(x_dim=10, y_dim=1, h_dim=5)


fileData = str(open("C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/curr_child.txt").read())
data = fileData.split()

# Path will need update depending on machine
model.openModel("C:/Users/dwrig/Desktop/Git/magicmachinelearning/model_json/gen" + data[1] + "/ind" + data[0])

# Path will need update depending on machine
x = magic.getAttackTensor('C:/Users/dwrig/Desktop/attackers.json')

guess = model.prediction(x)

print (guess)