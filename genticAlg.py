import numpy as np
import random
import json
import magic
import birnn
import os

# Edit these for your machine
prePath = 'C:/Users/dwrig/Desktop/fitnessPre.json'
postPath = 'C:/Users/dwrig/Desktop/fitnessPost.json'

# Training Parameters
pop_size = 10
mutate_chance = .5
x_dim = 10
y_dim = 1
h_dim = 5

# Fitness weights
survival = .75
kills = .5
damage = 1

#********************************************************************
# Function:     initJson
#
# Description:  Initializes fitness Json for Xmage game
#
# Parameters:   none
#
# Returned:     none
#********************************************************************
def initJson(path):
  data = {}

  data['turnNum'] = 0

  with open(path, 'w') as outfile:
    json.dump(data, outfile)

#********************************************************************
# Function:     saveFitness
#
# Description:  saves fitness from after match
#
# Parameters:   path    - path to save to
#               fitness - fitness to output
#
# Returned:     none
#********************************************************************
def saveFitness(path, fitness):
  fit = {}
  fit['fitness'] = fitness

  with open(path, 'w') as outfile:
    json.dump(fit, outfile)

#********************************************************************
# Function:     getMatchFittness
# 
# Description:  Calculate fitest model from generation
#
# Parameters:   none
#
# Returned:     fitness for current game 
#********************************************************************
def getMatchFittness(precombat, postcombat):
  preCom = magic.getJson(precombat)['data']
  postCom = magic.getJson(postcombat)['data']

  fittness = 0

  for i in range(len(preCom)):
    fittness += (preCom[i]['oppLife'] - postCom[i]['oppLife']) * damage
    fittness += (preCom[i]['aiCre'] - postCom[i]['aiCre']) * survival
    fittness += (preCom[i]['oppCre'] - postCom[i]['oppCre']) * kills

  return fittness


#********************************************************************
# Function:     nextGen
#
# Description:  Generates the next generation of models and saves
#               to new gen folder
#
# Parameters:   par1 - Parent 1 to base next gen off of (flattened param vector)
#               par2 - PArent 2 to base next gen off of (flattened param vector)
#
# Returned:     none
#********************************************************************
def nextGen(par1, par2):
  # Combine parents
  merged = np.append(par1[:len(par1)/2], par2[len(par2)/2:len(par2)])

  # Generate Mutated Children
  mutate = np.random.rand(len(merged))

  for j in range(pop_size):
    child = merged

    for i in range(len(merged)):
      if mutate[i] > mutate_chance:
        child[i] += np.random.rand() * [-1,1][random.randrange(2)]
    
    model = birnn.BiRNN(x_dim=x_dim, y_dim=y_dim, h_dim=h_dim)
    model.unFlattenWm(child)

    # Save to new generation directory
    i = 1
    while os.path.exists('./model_json/gen' + str(i)):
      i += 1
    
    os.makedirs('./model_json/gen' + str(i))

    model.saveModel('./model_json/gen' + str(i) + '/ind' + str(j) + '.json')